from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Schedule(models.Model):
    day=models.CharField(max_length=10)
    date=models.DateField(max_length=30)
    time=models.TimeField(max_length=17)
    activity=models.CharField(max_length=100)
    place=models.CharField(max_length=100)
    category=models.CharField(max_length=50)

    