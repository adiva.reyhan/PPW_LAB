from django import forms
from .models import Schedule
class ScheduleForm(forms.Form):
    time=forms.TimeField(label="Time", widget=forms.TimeInput(attrs={"type":"time"}))
    day=forms.CharField(label="Day", max_length=10)
    date=forms.DateField(label="Date", widget=forms.DateInput(attrs={"type":"date"}))
    activity=forms.CharField(label="Activity", max_length=100)
    place=forms.CharField(label="Place", max_length=100)
    category=forms.CharField(label="Category", max_length=50)




