from django.shortcuts import render, redirect
from django.http import HttpResponse,HttpResponseRedirect
from .forms import ScheduleForm
from .models import Schedule


# Create your views here.
response={}
def home(request):
    return render(request,"home.html",{})

def now(request):
    return render(request,"now.html", {})

def schedule(request):
    form=ScheduleForm(request.POST or None)
    if request.method=="POST":
        if form.is_valid():
          day=request.POST.get('day')
          date=request.POST.get('date')
          time=request.POST.get('time')
          activity=request.POST.get('activity')
          place=request.POST.get('place')
          category=request.POST.get('category')
          schedule=Schedule(day=day, date=date, time=time, activity=activity, place=place, category=category)
          schedule.save()
          return redirect("recap")
        else:
            response={}
            response['form']=form
            return render(request,"schedule.html", response)
    else:
        response={}
        response['form']=form
        return render(request,"schedule.html", response)

def recap(request):
    response={}    
    scheds=Schedule.objects.all()
    response['scheds']=scheds
    return render(request, 'recap.html', response)

def delete(request):
    response={}
    scheds=Schedule.objects.all().delete()
    return render(request, 'recap.html', response)

def curious(request):
    return render(request,"curious.html", {})

def achievements(request): 
    return render(request,"achievements.html", {})

def about(request):
    return render(request,"about.html", {})

def form(request):
    return render(request, "form.html", {})
