"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab4.views import home as index
from django.conf.urls.static import static
from django.conf import settings
from lab6 import views




urlpatterns = [
    re_path(r'^$', index , name='index'),
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^lab4/', include('lab4.urls')),
    re_path(r'^lab6/', include('lab6.urls')),
    re_path(r'^lab9/', include('lab9.urls')),
    re_path(r'^lab10/', include('lab_10.urls')),
    re_path(r'^lab11/', include('lab_11.urls')),
]+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)