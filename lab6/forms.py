from django import forms
from .models import Status

class StatusForm(forms.Form):
	attrs = {
		'class':'form-control',
		'placeholder':'I am feeling.....',
		'required':True,
	}

	stats = forms.CharField(label = "Write your thoughts here...", max_length = 300, widget=forms.TextInput(attrs=attrs))




