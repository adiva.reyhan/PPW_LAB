from django.shortcuts import render, redirect
from django.http import HttpResponse,HttpResponseRedirect
from .forms import StatusForm
from .models import Status

# Create your views here.
response={}
def index(request):
	form=StatusForm(request.POST or None)
	stats=Status.objects.all().order_by("time")[::-1]
	response["stats"]=stats
	response["form"]=form
	if(request.method=="POST" and form.is_valid()):
		stats = request.POST.get("stats")
		status=Status(stats=stats)
		status.save()
		return redirect('/')
	else:
		return render(request, "lab6.html", response)
		
def profile(request):
	return render(request,"profile.html", response)