import unittest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profile
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
import time

# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_lab6_url_exist(self):
		response = Client().get('/lab6/')
		self.assertEqual(response.status_code, 200)

	def test_lab6_using_index_func(self):
		found = resolve('/lab6/')
		self.assertEqual(found.func, index)

	def test_html_template(self):
		request=HttpRequest()
		response=index(request)
		html_response=response.content.decode('utf8')
		self.assertIn('Hello apa kabar?', html_response)

	def test_model_can_creat_new_status(self):
		new_status = Status.objects.create(stats="currently trying to be independent when learning")

		counting_all_status = Status.objects.all().count()
		self.assertEqual(counting_all_status, 1)

	def test_profile_url_exist(self):
		response=Client().get('/lab6/profile/')
		self.assertEqual(response.status_code, 200)

	def test_profile_using_index_func(self):
		found=resolve('/lab6/profile/')
		self.assertEqual(found.func, profile)

	def test_header_profile(self):
		request=HttpRequest()
		response=profile(request)
		html_response=response.content.decode('utf8')
		self.assertIn('WELCOME.', html_response)

	def test_profile_picture_exist(self):
		request=HttpRequest()
		response=profile(request)
		html_response=response.content.decode('utf8')
		self.assertIn("https://scontent.fcgk4-2.fna.fbcdn.net/v/t1.0-9/42059000_2225909457653072_3697286793914220544_n.jpg?_nc_cat=101&oh=2b520c2a707f063a039f1c5c17496d36&oe=5C21A05B", html_response)

	def test_short_description_exist(self):
		request=HttpRequest()
		response=profile(request)
		html_response=response.content.decode('utf8')
		self.assertIn("<p>It's me Adiva</p>", html_response)

class Lab6FunctionalTest(LiveServerTestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options )
		super(Lab6FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab6FunctionalTest, self).tearDown()

	# def test_post_a_status(self):
	# 	""" 
	# 		to test whether we 
	# 		can input new status or not and 
	# 		does the status appear in the web
	# 	"""
	# 	selenium = self.selenium
	# 	selenium.get('http://adivareyhanp-ppw-f.herokuapp.com/lab6/')
	# 	status_box=selenium.find_element_by_name('stats')
	# 	status_box.send_keys('Coba Coba')
	# 	status_box.submit()
	# 	self.assertIn("Coba Coba", selenium.page_source)

	# def test_title_is_correct(self):
	# 	"""
	# 		to test whether 
	# 		the website has the correct
	# 		title
	# 	"""
	# 	selenium=self.selenium
	# 	selenium.get('http://adivareyhanp-ppw-f.herokuapp.com/lab6/')
	# 	self.assertIn("Daily Rant", selenium.title)

	# def test_header_is_correct(self):
	# 	"""
	# 		to test whether the header
	# 		is correct or not
	# 	"""
	# 	selenium=self.selenium
	# 	selenium.get('http://adivareyhanp-ppw-f.herokuapp.com/lab6/')
	# 	heading=selenium.find_element_by_name('hello')
	# 	self.assertIn("Hello apa kabar", heading)

	# def test_style_speech_bubble(self):
	# 	"""
	# 		to test whether the 
	# 		speech bubble is in the right class 
	# 		or not
	# 	"""
	# 	selenium=self.selenium
	# 	selenium.get('http://adivareyhanp-ppw-f.herokuapp.com/lab6/')
	# 	bubble=selenium.find_element_by_tag_name('td')
	# 	self.assertIn("speech-bubble", bubble.get_attribute('class'))

	# def test_style_button(self):
	# 	""" 
	# 		to test whether
	# 		the button has the right
	# 		value or not
	# 	"""
	# 	selenium=self.selenium
	# 	selenium.get('http://adivareyhanp-ppw-f.herokuapp.com/lab6/')
	# 	button=selenium.find_element_by_name("button")
	# 	self.assertIn("Submit", button.get_attribute("value"))
