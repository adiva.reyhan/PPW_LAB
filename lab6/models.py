from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):
	stats=models.CharField(max_length = 300)
	time=models.DateTimeField(default=timezone.now())

def __str__(self):
	return self.stats