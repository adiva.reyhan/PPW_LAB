var category = "quilting";
$(document).ready(function(){
	getStar();
	var category = "quilting";
	$("#quilting").on("click", function(){
		var category = $(this).text();
		$.ajax({
			url: "data/" + category,
			datatype: 'json',
			success : function(data){
				renderHTML(data);
			},
			error : function(error){
				alert("Books not found");
			}
		})
	});
	$("#architecture").on("click", function(){
		var category = $(this).text();
		$.ajax({
			url: "data/" + category,
			datatype: 'json',
			success : function(data){
				renderHTML(data);
			},
			error : function(error){
				alert("Books not found");
			}
		})
	});
	$("#comic").on("click", function(){
		var category = $(this).text();
		$.ajax({
			url: "data/" + category,
			datatype: 'json',
			success : function(data){
				
				renderHTML(data);
			},
			error : function(error){
				alert("Books not found");
			}
		})
	});
	$.ajax({
		url: "data/" + category,
		datatype: 'json',
		success : function(data){
			renderHTML(data);
		},
		error : function(error){
			alert("Books not found");
		}
	});

	function renderHTML(data){
		htmlstring = "<tbody>";
		$("tbody").text("");
		console.log(data);
		for(i = 0; i < data.items.length; i++){
			htmlstring += 
			// "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"></img>" + "</td>" + 
			"<td><b>Title</b>:"+ data.items[i].volumeInfo.title + 
			"<br><b>Authors</b>:"+ data.items[i].volumeInfo.authors +
			"<br><b>Publisher</b>:"+ data.items[i].volumeInfo.publisher +
			"<br><b>Date</b>:"+ data.items[i].volumeInfo.publishedDate +"</td>"+
			"<td class='align-middle text-center' style='text-align:center'>" + "<img id='star" + i + "' onclick='favorite(this.id)' width='40px' src='https://image.flaticon.com/icons/svg/1218/1218365.svg'>" + "</td>"+
			"<td></td></tr>";
		}
		$('tbody').append(htmlstring);
		getStar();
	}
});

var counter = 0;
function getStar(){
	$.ajax({
		type : 'GET',
		url : '/lab9/getStar/',
		datatype : 'json',
		success : function(data){
			for(var i=1; i<=data.message.length; i++){
				console.log(data.message[i-1]);
				var id = data.message[i-1];
				var td = document.getElementById(id);
				if(td!=null){
					td.className='clicked';
					td.src = 'https://image.flaticon.com/icons/png/512/1218/1218513.png';
				}
				$('#counter').html(data.message.length);
			}
			// $('tbody').html(print);
		}
	});
};
	
function favorite(id){
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	var pick = document.getElementById(id);
	var clicked = 'https://image.flaticon.com/icons/png/512/1218/1218513.png';
	var unclicked = 'https://image.flaticon.com/icons/svg/1218/1218365.svg';
	if(pick.className=='clicked'){
		$.ajax({
			url:'/lab9/substract/',
			type: "POST",
			headers:{
				"X-CSRFToken" : csrftoken,

			},
			data : {
				id: id,
			},
			success : function(result){
				counter = result.message;
				pick.className='';
				pick.src=unclicked;
				$('#counter').html(counter);
			},
			error : function(msg){
				alert("Something went wrong");
			}
		});
	}else{
		$.ajax({
			url : "/lab9/add/",
			type :"POST",
			headers: {
				"X-CSRFToken" : csrftoken,
			},
			data: {
				id : id,
			},
			success:function(result){
				console.log(pick);
				counter = result.message;
				pick.className="clicked";
				pick.src=clicked;
				$('#counter').html(counter);
			},
			error : function(msg){
				alert("Something went wrong");
			}
		});
	}
}	
	