from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = [
    path('',views.landingpage, name='landingpagelab9'),
    path('data/<str:category>', views.data, name ='data'),
    path('add/', views.add, name="add"), 
    path('substract/', views.substract, name="substract"),
    path('getStar/', views.getStar, name="getStar"), 

]
