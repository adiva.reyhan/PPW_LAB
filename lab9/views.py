from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
import requests
import json

# Create your views here.
response={}
def data(request, category):
    json_read = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + category).json()
    return JsonResponse(json_read)

def landingpage(request):
    if request.user.is_authenticated and "counter" not in request.session:
        request.session['user'] = request.user.username 
        request.session['email'] = request.user.email
        request.session['sessionId'] = request.session.session_key
        request.session['counter'] = []
    return render(request, "landingpage.html")

@csrf_exempt
def add(request): 
    if(request.method=="POST"):
        stars = request.session["counter"]
        if request.POST["id"] not in stars:
            stars.append(request.POST["id"])
        print(request.session['sessionId'])
        print(request.session['counter'])
        request.session['counter'] = stars
        response["message"]=len(stars)
        return JsonResponse(response)
    else:
        return HttpResponse("Method not allowed")

@csrf_exempt
def substract(request):
    if(request.method == "POST"):
        stars = request.session["counter"]
        if request.POST["id"] in stars:
            stars.remove(request.POST["id"])
        print(request.session['sessionId'])
        print(request.session["counter"])
        request.session["counter"] = stars
        response["message"] = len(stars)
        return JsonResponse(response)
    else:
        return HttpResponse("Method not allowed")

def getStar(request):
    if request.user.is_authenticated:
        if(request.method == "GET"):
            if request.session["counter"] is not None:
                response["message"] = request.session["counter"]
            else:
                response["message"] = "NOT ALLOWED"
        else:
            response["message"]=""
        return JsonResponse(response)


