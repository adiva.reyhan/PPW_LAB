import unittest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from .views import landingpage, data

# Create your tests here.
class Lab9UnitTest(TestCase):

    def test_lab9_url_exist(self):
        response = Client().get('/lab9/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab9_using_index_func(self):
        found = resolve('/lab9/')
        self.assertEqual(found.func, landingpage)

    def test_lab9_html_template(self):
        response = Client().get('/lab9/')
        self.assertTemplateUsed(response, "landingpage.html")
    
    def test_json(self):
        response = Client().get("/lab9/data/quilting")
        self.assertEqual(response.status_code, 200)
    
    def test_url_add_exist(self):
        response = Client().get("/lab9/add/")
        self.assertEqual(response.status_code, 200)
    
    def test_url_substract_exist(self):
        response = Client().get("/lab9/substract/")
        self.assertEqual(response.status_code, 200)
        
    
    

class Lab9FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(Lab9FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab9FunctionalTest, self).tearDown()
    
    # def test_layout_1(self):
    #     selenium = self.selenium
    #     selenium.get("http://localhost:8000/lab9/")
    #     self.assertIn("book list", selenium.title)
    
    # def test_css(self):
    #     selenium = self.selenium
    #     selenium.get("http://localhost:8000/lab9/")
    #     header = selenium.find_element_by_id("header")
    #     css = header.value_of_css_property('text-align')
    #     self.assertEquals("center", css)