var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if(document.cookie.length > 0){
        c_start = document.cookie.indexOf(c_name + "=");
        if(c_start != -1){
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrftokenSafeMethod(method){
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function validEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

$(document).ready(function(){
    $.ajaxSetup({
        beforeSend: function(xhr, settings){
            if(!csrftokenSafeMethod(settings.type) && !this.crossDomain){
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

$("form").submit(function(event){
    $.ajax({
        method: "POST",
        url: 'subscribe/',
        data: {
            'name' : $("#id_name").val(),
            'email' : $("#id_email").val(),
        },
        dataType: 'json',
        success: function(msg){
            $( "#dialog" ).dialog();
                $( "#dialog" ).html("<p>You are now subscribed</p>");
                $("#id_name").val('');
                $("#id_email").val('');
        },
        error: function(msg){
            alert('Email not saved');
        },

    }),
    event.preventDefault();
});

$("#id_email").change(function(){
    email = $(this).val();
    $.ajax({
        method: "POST",
        url: 'check/',
        data: {
            'email': email
        },
        dataType: 'json',
        success: function(data){
            if(validEmail(email)==false){
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
            }
            else if (data.isTaken) {
                alert("Email already exist");
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
            }
            else{
                $("#submit")[0].disabled=false;
                $('#id_email').css("border-color", "green");
            }
        }
    });
});

$.ajax({
    url : "getSubscriber/",
    dataType : 'json',
    success: function(data){
        console.log(data.subs)
        $('tbody').html('')
        var stringHtml= '<tr>';
        for(var i=0 ; i<data.subs.length; i++){
            stringHtml += "<td>" + data.subs[i].name + "</td>" +
            "<td>" + data.subs[i].email + "</td>" +
            "<td style='text-align:center'>" + "<button id='unsub' onclick='unsubscriber("+ data.subs[i].id + ")' type='button' class='btn btn-danger'>unsubscribe</button"+"</td></tr>";
            }
            $('tbody').append(stringHtml);
        },
    })
});

function unsubscriber(id){
    console.log(id)
    $.ajax({
        url : "unsubscriber/",
        type : "POST",
        data : {
            'id' : id
        },
        success : function(id){
            window.location.reload(true);
        }
    });
}


