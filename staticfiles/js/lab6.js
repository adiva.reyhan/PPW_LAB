$(document).ready(function(){
$(".accordion").on("click", ".accordion-header", function() {
    $(this).toggleClass("active").next().slideToggle();
 });
 $("#theme").on({
   click: function(){
    $("body").css("background-color", "#E8F1F2");
    $("#theme").css("background-color", "#A6C9DD");
    $("#welcome").css("background-color", "#A6C9DD");
    $(".accordion-header:hover").css("background", "#A6C9DD");
    // $(".speech-bubble").css("background", "#A6C9DD");
    // $(".active.accordion-header::before").css("background-color", "#A6C9DD" );
   },
   dblclick: function(){
    $("body").css("background-color", "white");
    $("#theme").css("background-color", "#715D9A");
    $("#welcome").css("background-color", "white");
    $(".accordion-header:hover").css("background", "#E5C9EA");
    // $(".speech-bubble").css("background", "#ebefea");
    // $(".active.accordion-header::before").css("background-color", "#A6C9DD" );
   }
 });
})