// $(document).ready(function(){
//     $('#submit').prop('disabled', true);
//     $('#form-name').hide();
//     $('#form-email').hide();
//     $('#form-email-exist').hide();
//     $('#form-password').hide();
//     $('#name').val("");
//     $('#email').val("");
//     $('#password').val("");

//     var status = [false, false, false, false]
//     $('#name').on('input', function(){
//         var input = $(this);
//         check(input, 0);
//         checkButton();
//     });

//     var timer = null;
//     $('#email').keydown(function (){
//         clearTimeout(timer);
//         timer = setTimeout(function(){
//             var input = $("#email");
//             check(input, 1);
//             checkButton();
//         }, 1000)
//     });

//     $("#password").on('input', function(){
//         var input = $(this);
//         check(input, 2);
//         checkButton();
//     });

//     var check = function(input, arr){
//         if(arr === 1){
//             var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//             var is_data = reg.test(input.val());
//             if (is_data){
//                 $('#form-email').hide();
//                 checkEmail(input.val());
//                 status[arr] = true;
//                 return
//             }
//             else{
//                 $('#form-email-exist').hide();
//             }
//         }
//         else{
//             var is_data = input.val();
//         }
//         if(is_data){
//             if(arr === 0){
//                 $('#form-name').hide();
//             }
//             else if(arr === 1){
//                 $('#form-email').hide();
//             }
//             else{
//                 $('#form-password').hide();
//             }
//             status[arr]=true;
//         }
//         else{
//             if(arr === 0){
//                 $('#form-name').show();
//             }
//             else if(arr === 1){
//                 $('#form-email').show();
//             }
//             else{
//                 $('#form-password').show();
//             }
//             status[arr]=false;
//         }
//     };

//     var checkEmail = function (email){
//         var csrftoken = $("[name=csrfmiddlewaretoken").val();
//         $.ajax({
//             method: "POST",
//             url: "check/",
//             headers:{
//                 "X-CSRFToken" : csrftoken
//             },
//             data: {email: email},
//             success: function(response){
//                 if(response.is_email){
//                     $("#form-email-exist").show();
//                     status[3] = false;
//                     checkButton();
//                 }
//                 else{
//                     $('#form-email-exist').hide();
//                     status[3] = true;
//                     checkButton();
//                 }
//             },
//             error : function(error){
//                 alert("Data cannot be retrieved")
//             }
//         })
//     };
    
//     var checkButton = function(){
//         var button = $('#submit');
//         for(var i = 0; i< status.length; i++){
//             if(status[i] === false){
//                 button.prop('disabled', true);
//                 return
//             }
//         }
//         button.prop('disabled', false);
//     };

//     $(function (){
//         $('subscriber-form').on('submit', function(i){
//             i.preventDefault();
//             $.ajax({
//                 method: 'POST',
//                 url: '',
//                 data: $('form').serialize(),
//                 success : function (status){
//                     if(status){
//                         alert("You have subscribed to my website!");
//                     }
//                     else{
//                         alert("Sorry, something went wrong. Try again later");
//                     }
//                     for(var j = 0; j<status.length; j++){
//                         status[j]=false;
//                     }
//                     $("#submit").prop("disabled", true);
//                     $("#name").val("");
//                     $("#email").val("");
//                     $("#password").val("");
//                 },
//                 error: function(error){
//                     alert("Error")
//                 }
//             });
//         });
//     });
// });

var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if(document.cookie.length > 0){
        c_start = document.cookie.indexOf(c_name + "=");
        if(c_start != -1){
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrftokenSafeMethod(method){
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function validEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

$(document).ready(function(){
    $.ajaxSetup({
        beforeSend: function(xhr, settings){
            if(!csrftokenSafeMethod(settings.type) && !this.crossDomain){
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

$("form").submit(function(event){
    $.ajax({
        method: "POST",
        url: 'subscribe/',
        data: {
            'name' : $("#form-name").val(),
            'email' : $("#form-email").val(),
            'password' : $("#form-password").val(),
        },
        dataType: 'json',
        success: function(msg){
            $( "#mssg" ).dialog();
                $( "#mssg" ).html("<p>"+msg['mssg']+"</p>");
                $("#form-name").val('');
                $("#form-email").val('');
                $("#form-password").val('');
        },
            error: function(msg){
                alert("Something's wrong, try again later");
        },
    }),
    event.preventDefault();
});

$("#form-email").change(function(){
    email = $(this).val();
    $.ajax({
        method: "POST",
        url: 'check/',
        data: {
            'email': email
        },
        dataType: 'json',
        success: function(data){
            if(validEmail(email)==false){
                $('#form-email').css("border-color", "red");
                $('#submit')[0].disabled=true;
                $('#warnings').html('Enter a correct email address');
            }
            else if (data.isTaken) {
                alert("Email is taken");
                $('#form-email').css("border-color", "red");
                $("#submit")[0].disabled=true;
                $('#warnings').html('Use other email address');
            }
            else{
                $("#submit")[0].disabled=false;
                $('#id_email').css("border-color", "white");
                $('#warnings').html('');
            }
        }
    });
})
})

