from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
import json

# Create your views here.
def auths(request):
    response = {}
    return render(request, 'auth.html', response)

def logoutPage(request):
    request.session.flush()
    logout(request)
    return render(request, 'auth.html', {})