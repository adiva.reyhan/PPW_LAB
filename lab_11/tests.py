from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from lab9 import views as lab9Views
from .views import *
import unittest


# Create your tests here.
class Lab11TUnitTest(TestCase):
    def test_lab11_url_exist(self):
        response = Client().get('/lab11/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab11_using_index_func(self):
        found = resolve('/lab11/')
        self.assertEqual(found.func, auths)

    def test_lab11_html_template(self):
        response = Client().get('/lab11/')
        self.assertTemplateUsed(response, 'auth.html')
    
    def test_lab11_url_logout_exist(self):
        response = Client().get('/lab11/logout/')
        self.assertEqual(response.status_code, 200)
    
    
