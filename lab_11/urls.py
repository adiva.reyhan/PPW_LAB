from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf import settings
from django.contrib.auth import views
from .views import auths, logoutPage


# app_name= 'lab_11'
urlpatterns = [
    path('', auths, name='auths'),
    path('login/',  views.LoginView.as_view(), name="login"),
    # path('logout/', views.LogoutView.as_view(),  {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('logout/', logoutPage,  name ='logout'), #this
    path('auth/', include('social_django.urls', namespace='social')),
    
]