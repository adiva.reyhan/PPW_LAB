from django import forms

class SubscriberForm (forms.Form):
    name = forms.CharField(label = 'n a m e', required = True, max_length = 30, widget = forms.TextInput())
    email = forms.EmailField(label ='e m a i l', required = True, max_length = 30, widget = forms.TextInput())