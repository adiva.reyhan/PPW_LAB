import unittest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from .models import Subscriber
from .views import subscribe, check, display, getSubscriber, unsubscriber


#Create your tests here.
class Lab10UnitTest(TestCase):

    def test_lab10_url_exist(self):
        response = Client().get('/lab10/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab10_using_index_func(self):
        found = resolve('/lab10/')
        self.assertEqual(found.func, display)
    
    def test_lab10_html_template(self):
        request = HttpRequest()
        response = display(request)
        html_response = response.content.decode("utf8")
        self.assertIn("Subscribe Me!", html_response)
    
    def test_model_can_create_add_subscriber(self):
        new_subscriber = Subscriber.objects.create(name = "Adiva", email = "cobacoba@gmail.com")
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)
    
    def test_lab10_url_2_exist(self):
        response = Client().get('/lab10/check/')
        self.assertEqual(response.status_code,200)
        response = check(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('false', html)

    def test_if_posted(self):
        name = 'adiva'
        email = 'adiva@coba.com'
        response_post = Client().post('/lab10/subscribe/',{'name' : name,'email':email})
        self.assertEqual(response_post.status_code, 200)
        


class Lab10FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(Lab10FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab10FunctionalTest, self).tearDown()