from django.contrib import admin
from django.urls import path
from . import views

app_name= 'lab_10'
urlpatterns = [
    path('', views.display, name='display'),
    path('subscribe/', views.subscribe, name='subscribe'),
    path('check/', views.check, name='check'),
    path('getSubscriber/', views.getSubscriber, name="getSubscriber"),
    path('unsubscriber/', views.unsubscriber, name="unsubscriber"),   
]