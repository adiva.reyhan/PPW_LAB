from django.shortcuts import render
from django.http import JsonResponse
from django.db import IntegrityError
from .models import Subscriber
from .forms import SubscriberForm
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import requests
import json

# Create your views here.

def display(request):
    response = {}
    form = SubscriberForm(request.POST or None)
    response['form'] = form
    return render(request, 'subs.html', response)

def subscribe(request):
    response = {}
    try:
        if(request.method == "POST"):
            name = request.POST.get('name', None)
            email = request.POST.get('email', None)
            Subscriber.objects.create(name=name, email=email)
            return HttpResponse(json.dumps({'result' : 'You are now subscribed'}))
        else:
            return HttpResponse(json.dumps({'result' : 'failure'}))

    except IntegrityError as e:
        return HttpResponse(json.dumps({'result' : 'failure'}))

@csrf_exempt
def check(request):
    theEmail = request.POST.get('email', None)
    checking = {
        'isTaken' : Subscriber.objects.filter(email=theEmail).exists()
    }
    return JsonResponse(checking)

def getSubscriber(request):
    subs = Subscriber.objects.all().values()
    sub_list = list(subs)
    return JsonResponse({'subs': sub_list})

@csrf_exempt
def unsubscriber(request):
    if(request.method == "POST"):
        identitas = request.POST['id']
        delete = Subscriber.objects.filter(id=identitas).delete()
        return render(request, 'subs.html')

